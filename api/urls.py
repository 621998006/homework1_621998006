from django.urls import path
from api import views 
urlpatterns = [
    path('', views.LoginView),
    path('home/', views.HomeView),
]